<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {


  public function __construct(){
				parent::__construct();
				$this->load->model('Product_Model','',TRUE);
				$this->load->helper('url');
	}

	public function index($id=1)
	{
		$first_four = $this->Product_Model->get_first_four_entries($id);
		echo($first_four);
		// echo base_url().'assets/';
	}

	public function get_product($id)
	{
		$result = $this->Product_Model->get_product($id);
		echo($result);
	}

	public function add_product($title, $description)
	{
		$result = $this->Product_Model->add_product($title, $description);
		echo($result);
	}

	public function edit($id, $title, $description)
	{
		$result = $this->Product_Model->update_product($id, $title, $description);
		echo($result);
	}

	public function delete_product($id=0)
	{
		$result = $this->Product_Model->delete_product($id);
		echo($result);
	}
}
