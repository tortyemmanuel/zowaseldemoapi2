<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {

        public function get_first_four_entries($id=0)
        {
                $query = $this->db->get('global_deal_listings', 4);
                return json_encode($query->result());
        }

        public function get_product($id=0)
        {
                $response = "";
                $this->db->where('id', $id);
                $query = $this->db->get('global_deal_listings');
                // $query = $this->db->query('select * from global_deal_listings where id='.$id);
                //SELECT * FROM `global_deal_listings` order by `id` desc;
                  if(count($query) < 0){
                    $response = array(
                                  "error" => "Product does not exist!",
                                );
                  }else{
                     $response = $query->result();
                  }
                return json_encode($response);
        }

        public function add_product($title="",$desc="")
        {
          $response = "";
          $data = array('title' => $title, 'description' => $desc);
          $result = $this->db->insert('global_deal_listings', $data);
          if($result){
            $response = array(
                          "Success" => "Product has been added!",
                        );
          }else{
            $response = array(
                          "Error" => "There was a problem adding the product!",
                        );
          }

          return json_encode($response);
        }

        public function update_product($id, $title, $description)
        {
          $response = "";
          $this->db->where('id', $id);
          $result = $this->db->update('global_deal_listings', array('title'=>$title,
                                                                   'description'=>$description));
          if($result){
            $response = array(
                          "Success" => "Product has been updated!",
                        );
          }else{
            $response = array(
                          "Error" => "There was a problem updating the product!",
                        );
          }

          return json_encode($response);

        }

        public function delete_product($id=0)
        {
          $response = "";
          $this->db->where('id', $id);
          $result = $this->db->delete('global_deal_listings');
          if($result){
            $response = array(
                          "Success" => "Product has been deleted!",
                        );
          }else{
            $response = array(
                          "Error" => "There was a problem deleting the product! Try again later",
                        );
          }

          return json_encode($response);
        }

}
